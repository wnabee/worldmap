#include<stdlib.h>
#include<locale.h>
#include<ncurses.h>
#include<math.h>
#include<vector>
#include<string>
#include<thread>
#include<functional>
#include <arpa/inet.h>

#include<iostream>
#include<fstream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

#define PI 3.1415926
#define roundToNearest 0.5
#define PINGTIMEOUT 1

using namespace std;

void cursesHeader();
void cursesFooter(vector<WINDOW*> windows);
void drawMapImage(WINDOW *window, vector <unsigned char> image, int width, int height);
void drawMarkers(WINDOW *window, vector<tuple<int,int,int>> locations);
void annihilateCursor(WINDOW *window, int posX, int posY);
void printIPaddresses(WINDOW *window, vector<string> addresses, int currentAddress,vector<tuple<int,int,int>>markers);
void printCoordinates(WINDOW *window, tuple<float,float> coordinates);
void drawIPsquare(WINDOW *window);
void loadAddresses(vector<string>&addresses, string addressfile);
void callPing(vector<tuple<int,int,int>>&markers, vector<string>IPs);
void loadMarkers(vector<tuple<int,int,int>>&markers, string addressfile, int screenWidth, int screenHeight);
void appendMarker(vector<tuple<int,int,int>>&markers, string addressfile, int screenWidth, int screenHeight);
void printCredits();

tuple<int,int> coordinateTransform(int screenWidth, int screenHeight, string line);
tuple<int,int> drawCursor(WINDOW *window, int keyStroke, int posX, int posY, int width, int height);

bool grabImage(vector<unsigned char> &image, string &filename, int &width, int &height);
bool grabIPcoordinates(string IP,string addressfile);

void deleteEntry(bool &navMode, int menuModePosition, vector<string>&IPaddresses,
			vector<tuple<int,int,int>>&markers,string addressfile);
void addEntry(WINDOW* window,vector<tuple<int,int,int>>&markers,vector<string>&IPs,
			string addressfile,int screenWidth, int screenHeight);
int main(){
	
	setlocale(LC_ALL, "");

	string mapfile = string(PROGRAMDIR)+"maps/Worldmap150x54.png";
	string addressfile = string(PROGRAMDIR)+"CoordinateList.txt";

	vector<unsigned char> map;
	int mapWidth, mapHeight;
	bool grabSuccess = grabImage(map, mapfile, mapWidth, mapHeight);
	if(!grabSuccess){
		printf("error in grabbing reference image\n");
		return 1;
	}
	cursesHeader(); //initialise ncurses
	printCredits();

	int maxCol, maxRow;
	getmaxyx(stdscr,maxRow,maxCol);
	int mapOffset = (maxCol-mapWidth)/2;
	
	WINDOW *mapWindow;
	WINDOW *cursorWindow;
	WINDOW *IPPopupWindow;
	WINDOW *IPListWindow; 
	mapWindow = newwin(mapHeight, mapWidth, 0, mapOffset);
	cursorWindow = newwin(mapHeight,mapWidth,0, mapOffset);
	IPPopupWindow = newwin(5,20,maxRow/2-7,maxCol/2-10);
	IPListWindow = newwin(maxRow,mapOffset,0,0);
	
	vector<WINDOW *> windowlist = {mapWindow,cursorWindow, IPPopupWindow, IPListWindow};
	vector<tuple<int,int,int>> markers; //{{2,2,1},{2,5,2},{2,8,3}}; illustrative examples (x,y,state)
	vector<string> IPaddresses;
	loadAddresses(IPaddresses, addressfile);
	loadMarkers(markers, addressfile, mapWidth, mapHeight);
	
	keypad(cursorWindow, TRUE);

	refresh(); 

	int cursorX = maxCol/2;
	int cursorY = maxRow/2;
	int markerX,markerY;

	drawMapImage(mapWindow, map, mapWidth, mapHeight);
	drawMarkers(mapWindow,markers);
	wrefresh(mapWindow);

	thread setupPing{callPing,ref(markers),IPaddresses};
	setupPing.detach();

	bool navMode = 0;
	bool run = 1;
	int menuModePosition = 0;
		
	//end setup
	//start program

	while(run==1){
	
		int keyPress = wgetch(cursorWindow);
		switch(keyPress){
			case 9: //tab
				navMode = !navMode;
				break;
			case 113: //q
				run = 0;
				break;
			case 100: //d
				deleteEntry(navMode,menuModePosition,
						IPaddresses,markers,addressfile);
				break;
			case 97: //a
				addEntry(IPPopupWindow,markers,IPaddresses,
						addressfile,mapWidth,mapHeight);
				break;
			case 114: //r
				thread pingThread{callPing,ref(markers),IPaddresses};
				pingThread.detach();
				break;
		}
		
		
		wrefresh(IPPopupWindow);
		
		annihilateCursor(cursorWindow,cursorX,cursorY);
		wrefresh(cursorWindow);

		drawMapImage(mapWindow, map, mapWidth, mapHeight);
		drawMarkers(mapWindow,markers);
	
		werase(IPListWindow);

//-----------------------------------------------------------------------------Cancer containment zone
		if(navMode == 0){ //This toggles between free cursor movement (0) and menu based movement (1)
	  	tie(cursorX,cursorY) = drawCursor(cursorWindow,keyPress,cursorX,cursorY,mapWidth,mapHeight);
		printIPaddresses(IPListWindow,IPaddresses,-1,markers);
		}else{
			switch(keyPress){
				case KEY_UP:
					if(menuModePosition <= 0){
						menuModePosition = IPaddresses.size()-1;
					}else{
						menuModePosition=(menuModePosition-1)%IPaddresses.size();
					}
				break;
				case KEY_DOWN:
					menuModePosition=(menuModePosition+1)%IPaddresses.size();
				break;
			}
			drawCursor(mapWindow,0,get<0>(markers[menuModePosition]),
					get<1>(markers[menuModePosition]),mapWidth,mapHeight);
			printIPaddresses(IPListWindow,IPaddresses,menuModePosition,markers);
		}
//-----------------------------------------------------------------------------
		

		wrefresh(IPListWindow);
		wrefresh(mapWindow);
		wrefresh(cursorWindow);
	};

	cursesFooter(windowlist); //Always call this function to deallocate windows
	
	return 0;
}

void printCredits(){

	printw("Authored by Samuel Junesjo\n");
	printw("License: MIT \n");
	printw("Year: 2024\n\n");
	printw("Application to monitor\n");
	printw("the state of IP-addresses\n\n");
	printw("Keybinds:\n");
	printw("TAB - toggle navigation mode\n");
	printw("a - add an IP address\n");
	printw("d - delete an IP address,\n");
	printw("    only works in menu mode\n");
	printw("q - quit the program\n");
	printw("r - refresh ping\n");
	printw("arrow keys - navigate cursor\n\n");
	printw("press any key to continue\n");
	printw("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	printw("Note: Pings only refresh after \n");
	printw("a keypress. Markers being red \n");
	printw("right after being added is\n");
	printw("expected behaviour.\n\n");
}

bool grabImage(vector<unsigned char> &image, string &filename, int &width, int &height){

	int n;
   	unsigned char* data = stbi_load(filename.c_str(), &width, &height, &n, 4); //load the ref img
    	if (data != nullptr)
    	{	//We want to load our data into a vector to let us forget about memory management later.
    		image = std::vector<unsigned char>(data, data + width * height * 4); 
    	}
    	stbi_image_free(data);
   	return (data != nullptr);
}

void cursesHeader(){

	initscr();
	noecho();
	cbreak();
	start_color();
	use_default_colors();
	init_pair(1, COLOR_GREEN, COLOR_WHITE);
	init_pair(2, COLOR_YELLOW, COLOR_WHITE);
	init_pair(3, COLOR_RED, COLOR_WHITE);
	init_pair(4, COLOR_CYAN, COLOR_WHITE);
	init_pair(5, COLOR_BLUE, COLOR_WHITE);
	init_pair(6, COLOR_WHITE, COLOR_CYAN);
	init_pair(7, COLOR_BLACK, COLOR_WHITE);
}

void cursesFooter(vector<WINDOW *> windows){
	for(int i = 0; i < windows.size();i++){
	delwin(windows[i]);
	}
	endwin();	
}

void drawMapImage(WINDOW *window, vector<unsigned char> image, int width, int height){
	
	for(int x = 0; x < width; x++){
		for(int y = 0; y < height; y++){
			size_t index = 4 * (y*width + x);
			if(image[index+3] != 0){ 
				//This is rather suboptimal
				//but it works for now.
				
				wattron(window,COLOR_PAIR(4)); //Draw the map
				mvwprintw(window,y,x,"\u2588");
				wattroff(window,COLOR_PAIR(4));
				
				wattron(window,COLOR_PAIR(5)); //and the shadow
				mvwprintw(window,y+1,x,"\u2588");
				wattroff(window,COLOR_PAIR(5));
			}
		}
	}
}


tuple<int,int> drawCursor(WINDOW *window, int keyStroke, int posX, int posY, int width, int height){

	switch(keyStroke){
		case KEY_LEFT:
			if(posX > 0){posX--;} //conditionals for boundries.
			break;
		case KEY_RIGHT:
			if(posX < width){posX++;}
			break;
		case KEY_UP:
			if(posY > 0){posY--;}
			break;
		case KEY_DOWN:
			if(posY < height){posY++;}
			break;
	}
	
	mvwprintw(window,height-2,width-15, "%d,%d", posX,posY);
	mvwprintw(window,posY-1,posX,"\u2193");
	mvwprintw(window,posY+1,posX,"\u2191");
	mvwprintw(window,posY,posX+1,"\u2190");
	mvwprintw(window,posY,posX-1,"\u2192");

	return {posX,posY};
}

void drawMarkers(WINDOW *window, vector<tuple<int,int,int>> locations){
	
	int posX,posY,serverState;
	for(auto location : locations){
		tie(posX,posY,serverState) = location;
		wattron(window,COLOR_PAIR(6));
		mvwprintw(window,posY-1,posX-1,"\u250C");
		mvwprintw(window,posY+1,posX-1,"\u2514");
		mvwprintw(window,posY-1,posX+1,"\u2510");
		mvwprintw(window,posY+1,posX+1,"\u2518");
		wattroff(window,COLOR_PAIR(6));
		wattron(window,COLOR_PAIR(serverState));
		mvwprintw(window,posY,posX,"\u2588");
		wattroff(window,COLOR_PAIR(serverState));

	}
}

void annihilateCursor(WINDOW *window, int posX, int posY){

	mvwprintw(window,posY-1,posX,"\u0020");
	mvwprintw(window,posY+1,posX,"\u0020");
	mvwprintw(window,posY,posX+1,"\u0020");
	mvwprintw(window,posY,posX-1,"\u0020");
}

void drawIPsquare(WINDOW *window){
	
	box(window,0,0);

}

void printIPaddresses(WINDOW *window, vector<string> addresses, int currentAddress, vector<tuple<int,int,int>>markers){
	

	box(window,0,0);
	mvwprintw(window,2,3,"IP-address list <a>");
	int i = 0;
	for(string address : addresses){
		if(i == currentAddress){wattron(window,COLOR_PAIR(7));}
		mvwprintw(window, 4+i, 6, address.c_str());
		if(i == currentAddress){wattroff(window,COLOR_PAIR(7));}
		
		wattron(window,COLOR_PAIR(get<2>(markers[i])));
		mvwprintw(window, 4+i, 3, "\u2588");
		wattroff(window,COLOR_PAIR(get<2>(markers[i])));

		i++;
	}
}


bool grabIPcoordinates(string IP, string addressfile){ //hack to get the coordinates of an IP-address into a file
	
	string ipGrabCommand = "echo \""+IP+
		": $(geoiplookup "+IP+" | grep -oE \"GeoIP City Edition.*,"+
		" ([1-9]|[1-9][0-9]).*,.*,\" | cut -d, -f 7,8)\">> "+addressfile;

	bool grabSuccess = system(ipGrabCommand.c_str());
	
	return grabSuccess;
}

	
void loadAddresses(vector<string>&addresses, string addressfile){ //adds IP-address persistance

	string line;
	ifstream list (addressfile);
	if(list.is_open()){
		while(getline(list,line)){ 
			size_t colon = line.find(':');
			if(colon != string::npos){
				addresses.push_back(line.substr(0,colon));
			}
		}
		list.close();
	}else{
		string errormessage = "Warning: Failed to find addressfile: "+addressfile;
		printw("%s",errormessage);
	}
}

tuple<int,int> coordinateTransform(int screenWidth, int screenHeight, string line){

	double markerX = 0;
	double markerY = 0;
	double CropCorrection = screenHeight/5; //The top and bottom 10 or so degrees of the globe are uninteresting to display
						//but cropping these shifts everything in the far north closer to the equator.
						//CropCorrection should correct for this shift approximately.
	size_t colon = line.find(':');

	if(colon != string::npos){
		string coordinates = line.substr(colon+2,line.length()-colon);

		
		size_t comma = coordinates.find(',');
		if(comma != string::npos){
			double y = stod(coordinates.substr(1,comma-1));
			double x = stod(coordinates.substr(comma+2,coordinates.length()-(comma+2)));

			
			markerX = screenWidth*(x+180)/360+roundToNearest;
			markerY = -((screenHeight+CropCorrection)/(2*PI))*(log(tan(PI/4+PI/180*y/2)))-roundToNearest+screenHeight/2; 
		}
	}

	return tie(markerX,markerY);
}

void loadMarkers(vector<tuple<int,int,int>>&markers, string addressfile, int screenWidth, int screenHeight){
	
	string line;
	ifstream list (addressfile);
	if(list.is_open()){
		while(getline(list,line)){
			tuple<int,int> screenCoordinates = coordinateTransform(screenWidth, screenHeight, line);
			markers.push_back(make_tuple(get<0>(screenCoordinates),get<1>(screenCoordinates),3));
		}
		list.close();
	}
}

void appendMarker(vector<tuple<int,int,int>>&markers, string addressfile, int screenWidth, int screenHeight){

	string line;
	ifstream list (addressfile);
	if(list.is_open()){
		while(list >> ws && getline(list,line));
		tuple<int,int> screenCoordinates = coordinateTransform(screenWidth, screenHeight, line);
		markers.push_back(make_tuple(get<0>(screenCoordinates),get<1>(screenCoordinates),3));
		
		list.close();
	}
}

void callPing(vector<tuple<int,int,int>>&markers, vector<string>IPs){
	
	int pingTimeout = PINGTIMEOUT;
	int i = 0;
	for(auto IP : IPs){
		string ping = "ping -w "+to_string(pingTimeout)+" "+IP+" | grep -oPc \"(?<=transmitted, )0\" > /dev/null";
		bool response = system(ping.c_str());
		if(response == 0){
			get<2>(markers[i]) = 3;
		}else if(response == 1){
			get<2>(markers[i]) = 1;
		}
		i++;
	}
}

void deleteEntry(bool &navMode, int menuModePosition, vector<string> &IPaddresses,
				vector<tuple<int,int,int>> &markers,string addressfile){
	if(navMode == 1){
		IPaddresses.erase(IPaddresses.begin()+menuModePosition);
		markers.erase(markers.begin()+menuModePosition);
		string eraseLineCommand = "sed -i \'"+to_string(menuModePosition+1)+"d\' "+addressfile;
		system(eraseLineCommand.c_str());
	}
}

void addEntry(WINDOW* window,vector<tuple<int,int,int>>&markers,vector<string>&IPs,
			string addressfile,int screenWidth, int screenHeight){
	echo();
	werase(window);
	drawIPsquare(window);
	char newIP[15];
	unsigned char buf[32];

	mvwgetnstr(window,2,3,newIP,15);
	string IP = newIP;
	if(inet_pton(AF_INET, newIP, buf) == 1){
		grabIPcoordinates(IP,addressfile);
		appendMarker(markers,addressfile,screenWidth,screenHeight);
	}
	
	IPs.push_back(IP);
	noecho();
}
